<?php

class Progress extends \Phalcon\Mvc\Model
{
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @return int
     */
    public function getSubjectId()
    {
        return $this->subject_id;
    }

    /**
     * @return int
     */
    public function getGroupId()
    {
        return $this->group_id;
    }

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=10, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=false)
     */
    public $user_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=false)
     */
    public $subject_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=false)
     */
    public $group_id;

    /**
     *
     * @var double
     * @Column(type="double", nullable=false)
     */
    public $grade;

	/**
	 * @return float
	 */
	public function getGrade()
	{
		return $this->grade;
	}

	/**
	 * @return int
	 */
	public function getOmission()
	{
		return $this->omission;
	}

	/**
	 * @return int
	 */
	public function getSemester()
	{
		return $this->semester;
	}

	/**
	 * @return int
	 */
	public function getCourse()
	{
		return $this->course;
	}

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $omission;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $semester;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $course;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("journal");
        $this->belongsTo('group_id', '\Group', 'id', ['alias' => 'Group']);
        $this->belongsTo('subject_id', '\Subject', 'id', ['alias' => 'Subject']);
        $this->belongsTo('user_id', '\Users', 'id', ['alias' => 'Users']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'progress';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Progress[]|Progress
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Progress
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}

<?php

class Subject extends \Phalcon\Mvc\Model
{
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=10, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    public $name;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("journal");
        $this->hasMany('id', 'GroupSubject', 'subject_id', ['alias' => 'GroupSubject']);
        $this->hasMany('id', 'Progress', 'subject_id', ['alias' => 'Progress']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'subject';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Subject[]|Subject
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Subject
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}

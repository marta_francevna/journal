<header class="header">
	<div class="b-title-1">
		<span class="text">Электронный дневник</span>
	</div>
	<div class="b-title-2">
		<span class="text">Добавление предмета</span>
	</div>
</header>

<div class="main">
	<form method="post" action="/subjects/add" class="form-edit-inf">
		<label>Название:</label>
		{{ form.render('name') }}
	<div class="b-button button-custom button-edit-inf">
		<a href="/subjects" class="link-back">« Назад</a>
		<button type="submit" class="button">Создать</button>
	</div>
	<span class="errors">
			{% for message in form.getMessages() %}
				<p>{{ message.getMessage() }}</p>
			{% endfor %}
		</span>
	</form>
</div>

{#<h1>Добавление предмета</h1>#}
{#<form action="/subjects/add" method="post">#}

{#<label>Название:</label>#}
{#{{ form.render('name') }}#}
	{#<input type="submit" value="Создать">#}
{#</form>#}
{#<div id="errors">#}
	{#{% for message in form.getMessages() %}#}
		{#<p>{{ message.getMessage() }}</p>#}
	{#{% endfor %}#}
{#</div>#}
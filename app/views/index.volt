<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Электронный дневник</title>
    <link rel="stylesheet" href="/css/style.css">
    <script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
    <script src="/js/navmenu.min.js"></script>
    <script src="/js/jquery-1.8.2.min.js"></script>
</head>
<body class="b-page">

{{ content() }}
<!-- jQuery -->
<script src="/js/jquery.js"></script>
{#<script type="text/javascript">$('.menu-items').Menu()</script>#}
<!-- Bootstrap Core JavaScript -->
<footer class="footer">
    <div class="container">
        <span class="text">© 2017, Филиал "БГТУ" "ВитГТК". Все права защищены.</span>
    </div>
</footer>

</body>
</html>
<header class="header">
	<div class="b-title-1">
		<span class="text">Электронный дневник</span>
	</div>
	<div class="b-title-2">
		<span class="text">Редактирование прогресса</span>
	</div>
</header>

<div class="main">
	<form method="post" action="/progress/edit/{{ progress.getId() }}" class="form-edit-inf">
		{{ form.render('user_id') }}
		<label>Предмет:</label>
		{{ form.render('subject_id') }}
		<label>Ср.балл</label>
		{{ form.render('grade') }}
		<label>Пропуски</label>
		{{ form.render('omission') }}
		{{ form.render('course') }}
		{{ form.render('semester') }}
		<span class="errors">
			{% for message in form.getMessages() %}
				<p>{{ message.getMessage() }}</p>
			{% endfor %}
		</span>
		<div class="b-button button-custom button-edit-inf">
			<a href="/student/{{ session.get('user_id') }}/course/{{ session.get('course') }}/semester/{{ session.get('semester') }}" class="link-back">« Назад</a>
			<button type="submit" class="button">Сохранить</button>
		</div>

	</form>
</div>

{#<h1>Редактирование прогресса</h1>#}
{#<form action="/progress/edit/{{ progress.getId() }}" method="post">#}
	{#{{ form.render('user_id') }}#}
{#<label>Предмет:</label>#}
{#{{ form.render('subject_id') }}#}
{#<label>Ср.балл</label>#}
{#{{ form.render('grade') }}#}
{#<label>Пропуски</label>#}
{#{{ form.render('omission') }}#}
	{#{{ form.render('course') }}#}
	{#{{ form.render('semester') }}#}
	{#<input type="submit" value="сохранить">#}
{#</form>#}
{#<div id="errors">#}
	{#{% for message in form.getMessages() %}#}
		{#<p>{{ message.getMessage() }}</p>#}
	{#{% endfor %}#}
{#</div>#}
<script type="text/javascript" src="/js/create-progress.js"></script>

<header class="header">
	<div class="b-title-1">
		<span class="text">Электронный дневник</span>
	</div>
	<div class="b-title-2">
		<span class="text">Добавление прогресса</span>
	</div>
</header>
<div class="main">
	<div class="b-main">
		<form method="post" action="/progress/create" id="form">
			<div id="resp1">
				<span>Выберите группу:</span>
				<select name="group" id="group" onmousedown="$(':first-child', this).remove(); this.onmousedown = null;">
					<option value=""></option>
					{% for g in group %}
					<option>{{ g.name }}</option>
					{% endfor %}
				</select>
				<input name="year" id="year" type="number" min="1900" max="2099" step="1" value="2017"/>
				<div class="b-button">
					<input class="button" type="button" id="b1" value="Далее">
				</div>
				<span  class="errors">
					{% for message in form.getMessages() %}
						<p>{{ message.getMessage() }}</p>
					{% endfor %}
				</span>
			</div>
				<div id="response"></div>
				<div id="respon" style="display: none">
					<form action="/progress/create" method="post">
						<div id="resp2">
						</div>
						<label>Предмет:</label>
						{{ form.render('subject_id') }}
						<input type="hidden" name="group_id" value="{{ session.group_id }}">
						<label>Ср.балл</label>
						{{ form.render('grade') }}
						<label>Пропуски</label>
						{{ form.render('omission') }}
						<label>Семестр</label>
						{{ form.render('course') }}
						<label>Курс</label>
						{{ form.render('semester') }}
						<div class="b-button button-custom button-edit-inf">
							<a href="/info" class="link-back">«
								Назад</a>
							<button type="submit" class="button">Создать</button>
						</div>
					</form>

				</div>
		</form>
	</div>
</div>
{#<div id="resp1"> Выберите группу:#}
	{#<select name="group" id="group" onmousedown="$(':first-child', this).remove(); this.onmousedown = null;">#}
		{#<option value=0></option>#}
		{#{% for g in group %}#}
			{#<option>{{ g.name }}</option>#}
		{#{% endfor %}#}
	{#</select>#}
	{#<input name="year" id="year" type="number" min="1900" max="2099" step="1" value="2017"/>#}
	{#<input type="button" id="b1" value="ok">#}
{#</div>#}
{#<div id="response"></div>#}
{#<div id="respon" style="display: none">#}
	{#<h1>Добавление прогресса</h1>#}
	{#<form action="/progress/create" method="post">#}
		{#<div id="resp2">#}
		{#</div>#}
		{#<label>Предмет:</label>#}
		{#{{ form.render('subject_id') }}#}
		{#<input type="hidden" name="group_id" value="{{ session.group_id }}">#}
		{#<label>Ср.балл</label>#}
		{#{{ form.render('grade') }}#}
		{#<label>Пропуски</label>#}
		{#{{ form.render('omission') }}#}
		{#<label>Семестр</label>#}
		{#{{ form.render('course') }}#}
		{#<label>Курс</label>#}
		{#{{ form.render('semester') }}#}
		{#<input type="submit" value="Создать">#}
	{#</form>#}
	{#<div id="errors">#}
		{#{% for message in form.getMessages() %}#}
			{#<p>{{ message.getMessage() }}</p>#}
		{#{% endfor %}#}
	{#</div>#}
{#</div>#}


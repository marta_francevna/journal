<header class="header">
	<div class="b-title-1">
		<span class="text">Электронный дневник</span>
	</div>
	<div class="b-title-2">
		<span class="text">Добавление прогресса</span>
	</div>
</header>
<div class="main">
	<form method="post" action="/progress/add" class="form-edit-inf">
		<input type="hidden" name="user_id" value="{{ session.user_id }}">
		<input type="hidden" name="group_id" value="{{ session.group_id }}">
		<label>Предмет:</label>
		{{ form.render('subject_id') }}
		<label>Ср.балл</label>
		{{ form.render('grade') }}
		<label>Пропуски</label>
		{{ form.render('omission') }}
		<input type="hidden" name="course" value="{{ session.course }}">
		<input type="hidden" name="semester" value="{{ session.semester }}">
		<span class="errors">
			{% for message in form.getMessages() %}
				<p>{{ message.getMessage() }}</p>
			{% endfor %}
		</span>
		<div class="b-button button-custom button-edit-inf">
			<a href="/student/{{ session.get('user_id') }}/course/{{ session.get('course') }}/semester/{{ session.get('semester') }}" class="link-back">«
				Назад</a>
			<button type="submit" class="button">Создать</button>
		</div>

	</form>
</div>

{#<h1>Добавление прогресса</h1>#}
{#<form action="/progress/add" method="post">#}
	{#<input type="hidden" name="user_id" value="{{ session.user_id }}">#}
	{#<input type="hidden" name="group_id" value="{{ session.group_id }}">#}
{#<label>Предмет:</label>#}
{#{{ form.render('subject_id') }}#}
{#<label>Ср.балл</label>#}
{#{{ form.render('grade') }}#}
{#<label>Пропуски</label>#}
{#{{ form.render('omission') }}#}
	{#<input type="hidden" name="course" value="{{ session.course }}">#}
	{#<input type="hidden" name="semester" value="{{ session.semester }}">#}
	{#<input type="submit" value="Создать">#}
{#</form>#}
{#<div id="errors">#}
	{#{% for message in form.getMessages() %}#}
		{#<p>{{ message.getMessage() }}</p>#}
	{#{% endfor %}#}
{#</div>#}
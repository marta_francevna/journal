<header class="header">
	<div class="b-title-1">
		<span class="text">Электронный дневник</span>
	</div>
	<div class="b-title-2">
		<span class="text">Редактирование группы</span>
	</div>
</header>

<div class="main">
	<form method="post" action="/groups/edit/{{ group.getId() }}"  class="form-edit-inf">
		<label>Название</label>
		{{ form.render('name') }}
		<label>Год</label>
		{{ form.render('year') }}
		<label>Статус</label>
		{{ form.render('status') }}
		<div class="b-button button-custom button-edit-inf">
			<a href="/groups" class="link-back">« Назад</a>
			<button type="submit" class="button">Сохранить</button>
		</div>
		<span class="errors">
			{% for message in form.getMessages() %}
				<p>{{ message.getMessage() }}</p>
			{% endfor %}
		</span>
	</form>
</div>

{#<form action="/groups/edit/{{ group.getId() }}" method="post">#}
	{#<label>Название</label>#}
	{#{{ form.render('name') }}#}
	{#<label>Год</label>#}
	{#{{ form.render('year') }}#}
	{#<label>Статус</label>#}
	{#{{ form.render('status') }}#}
	{#<input type="submit" value="Сохранить">#}
{#</form>#}
{#<div id="errors">#}
	{#{% for message in form.getMessages() %}#}
		{#<p>{{ message.getMessage() }}</p>#}
	{#{% endfor %}#}
{#</div>#}
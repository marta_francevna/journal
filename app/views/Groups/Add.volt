<header class="header">
	<div class="b-title-1">
		<span class="text">Электронный дневник</span>
	</div>
	<div class="b-title-2">
		<span class="text">Добавление группы</span>
	</div>
</header>

<div class="main">
	<form method="post" action="/groups/add"  class="form-edit-inf">
		<label>Название</label>
		{{ form.render('name') }}
		<label>Год</label>
		{{ form.render('year') }}
		<label>Статус</label>
		{{ form.render('status') }}
		<div class="b-button button-custom button-edit-inf">
			<a href="/groups" class="link-back">« Назад</a>
			<button type="submit" class="button">Создать</button>
		</div>
		<span class="errors">
			{% for message in form.getMessages() %}
			<p>{{ message.getMessage() }}</p>
			{% endfor %}
		</span>
	</form>
</div>
{#<h1>Добавление группы</h1>#}
{#<form action="/groups/add" method="post">#}
{#<label>Название</label>#}
{#{{ form.render('name') }}#}
{#<label>Год</label>#}
{#{{ form.render('year') }}#}
{#<label>Статус</label>#}
{#{{ form.render('status') }}#}
	{#<input type="submit" value="Создать">#}
{#</form>#}
{#<div id="errors">#}
	{#{% for message in form.getMessages() %}#}
		{#<p>{{ message.getMessage() }}</p>#}
	{#{% endfor %}#}
{#</div>#}
<header class="header">
	<div class="b-title-1">
		<span class="text">Электронный дневник</span>
	</div>
	<div class="b-title-2">
		<span class="text">Основное</span>
	</div>
</header>
<nav class="navbar">
	<div class="b-navbar container">
		<ul class="menu-items">
			<li><a href="/info" class="active">Основное</a></li>
			<li><a href="/subjects">Предметы</a></li>
			<li><a href="/students">Ученики</a></li>
			<li><a href="/groups">Группы</a></li>
		</ul>
		<div class="b-login">
			<a href="/setting" class="login"> {{ session.get('login') }}</a>
			<a href="/logout" class="exit">Выход</a>
		</div>
	</div>
</nav>
<div class="main">
	<div class="b-main">
		<div class="b-text">
			<span class="text">Ученик: {{ student }}</span>
		</div>
		</form>
		<table>
			<thead>
				<tr>
					<th>Предмет</th>
					<th>Ср. оценка</th>
					<th>Кол-во пропусков</th>
					<th>Изм.</th>
					<th>Уд.</th>
				</tr>
			</thead>
			<tbody>
				{% for p in progress %}
				{% set sub = subjects[p.subject_id] %}
				<tr>
					<td>{{ sub.name }}</td>
					<td>{{ p.grade }}</td>
					<td>{{ p.omission }}</td>
					<td><a href="/progress/edit/{{ p.id }}" class="edit"></a></td>
					<td><a href="/progress/del/{{ p.id }}"  onclick="document.getElementById('myform-{{ p.id }}').submit(); return false;" class="delete"></a></td>
				</tr>
				<form method="post" action="/progress/del/{{ p.id }}" id="myform-{{ p.id }}">
				<input type="hidden" name="url" value="{{ router.getRewriteUri() }}">
				</form>
				{% endfor %}
			</tbody>
		</table>
		<a href="/progress/add/" class="book">Создать</a>
	</div>
</div>

{#<div class="table2">#}
	{#<h1>Результат:</h1>#}
	{#Ученик-{{ student }}#}
	{#<table border="1">#}
		{#<thead>#}
			{#<tr>#}
				{#<th>Предмет</th>#}
				{#<th>Средняя оценка</th>#}
				{#<th>Пропуски</th>#}
				{#<th>Удаление</th>#}
				{#<th>Редактирование</th>#}
			{#</tr>#}
		{#</thead>#}
		{#<tbody>#}
			{#{% for p in progress %}#}
				{#{% set sub = subjects[p.subject_id] %}#}
				{#<tr>#}
					{#<td>{{ sub.name }}</td>#}
					{#<td>{{ p.grade }}</td>#}
					{#<td>{{ p.omission }}</td>#}
					{#<td>#}
						{#<a href="/progress/del/{{ p.id }}"  onclick="document.getElementById('myform-{{ p.id }}').submit(); return false;">Удалить</a>#}
					{#</td>#}
					{#<td><a href="/progress/edit/{{ p.id }}">Редактировать</a></td>#}
				{#</tr>#}
				{#<form method="post" action="/progress/del/{{ p.id }}" id="myform-{{ p.id }}">#}
					{#<input type="hidden" name="url" value="{{ router.getRewriteUri() }}">#}
				{#</form>#}
			{#{% endfor %}#}
		{#</tbody>#}
	{#</table>#}
	{#<a href="/progress/add/">Создать</a>#}
{#</div>#}


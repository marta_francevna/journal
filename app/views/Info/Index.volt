<script type="text/javascript" src="/js/student-semestr.js"></script>
<script type="text/javascript" src="/js/kurator-course.js"></script>
{% if session.get('authorization') === 0 %}
	<header class="header">
		<div class="b-title-2">
			<span class="text">Электронный дневник</span>
		</div>
	</header>
	<nav class="navbar">
		<div class="b-navbar container">
			<div class="b-login">
				<a href="/setting" class="login"> {{ session.get('login') }}</a>
				<a href="/logout" class="exit">Выход</a>
			</div>
		</div>
	</nav>

{% elseif session.get('authorization') === 1 %}
	<header class="header">
		<div class="b-title-1">
			<span class="text">Электронный дневник</span>
		</div>
		<div class="b-title-2">
			<span class="text">Основное</span>
		</div>
	</header>
	<nav class="navbar">
		<div class="b-navbar container">
			<ul class="menu-items">
				<li><a href="/info" class="active">Основное</a></li>
				<li><a href="/subjects">Предметы</a></li>
				<li><a href="/students">Ученики</a></li>
				<li><a href="/groups">Группы</a></li>
			</ul>
			<div class="b-login">
				<a href="/setting" class="login"> {{ session.get('login') }}</a>
				<a href="/logout" class="exit">Выход</a>
			</div>
		</div>
	</nav>
{% endif %}
{% if session.get('authorization') === 0 %}
<div class="main">
	<div class="b-main">
		<form method="post" action="/info" id="form">
			<div>
				<span>Выберите курс:</span>
				<select name="course" id="course" onmousedown="$(':first-child', this).remove(); this.onmousedown = null;">
					<option value=""></option>
					{% for c in courses %}
						<option>{{ c.course }}</option>
					{% endfor %}
				</select>
				<div id="resp1"></div>
				<div id="resp" class="error" style="display: none"></div>
			</div>
			<div class="b-button button-custom">
			</div>
		</form>
		{% endif %}
		{% if (flag2===true) %}
			{% if  progr|length> 0 %}
				<div class="b-text">
					<span class="text"> Курс: {{ kur[0].course }}. Семестр: {{ sem[0].semester }}</span>
				</div>

				<table class="table2">
					<thead>
						<tr>
							<th>Предмет</th>
							<th>Ср. оценка</th>
							<th>Кол-во пропусков</th>
						</tr>
					</thead>
					<tbody>
						{% for p in progr %}
							{% set sub = subj[p.subject_id] %}
							<tr>
								<td>{{ sub.name }}</td>
								<td>{{ p.grade }}</td>
								<td>{{ p.omission }}</td>
							</tr>
						{% endfor %}
					</tbody>
				</table>
			{% else %}
				<div class="sorry">Извините, нет данных</div>
			{% endif %}
		{% endif %}
		{% if (flag===true) %}
			{% if  progress|length> 0 %}
				<div class="b-text">
					<span class="text"> Курс: {{ kur }}. Семестр: {{ sem }}</span>
				</div>
				<table class="table">
					<thead>
						<tr>
							<th>Предмет</th>
							<th>Ср. оценка</th>
							<th>Кол-во пропусков</th>
						</tr>
					</thead>
					<tbody>
						{% for p in progress %}
							{% set sub = subjects[p.subject_id] %}
							<tr>
								<td>{{ sub.name }}</td>
								<td>{{ p.grade }}</td>
								<td>{{ p.omission }}</td>
							</tr>
						{% endfor %}
					</tbody>
				</table>
			{% else %}
				<div class="sorry">Извините, нет данных</div>
			{% endif %}
		{% endif %}
	</div>
</div>
{% if session.get('authorization') === 1 %}
	<div class="main">
		<div class="b-main">
			<form method="post" action="/progress" id="form2">
				<div id="resp1">
					<span>Выберите группу:</span>
					<select name="group" id="group" onmousedown="$(':first-child', this).remove(); this.onmousedown = null;">
						<option value=0></option>
						{% for g in group %}
							<option>{{ g.name }}</option>
						{% endfor %}
					</select>
					<input name="year" id="year" type="number" min="1900" max="2099" step="1" value="2017"/>
					<div class="b-button">
						<input class="button" type="button" id="b1" value="Далее">
					</div>
				</div>
				<div id="resp2">

				</div>
				<div id="resp3">

				</div>
				<span id="response" class="error" style="display: none"></span>
			</form>
			<a href="/progress/create/" class="book">Создать</a>
		</div>
	</div>
{% endif %}
{#Привет, {{ user.name }}#}
{#<li><a href="/logout">Выйти</a></li>#}
{#{% if session.get('authorization') === 0 %}#}
	{#<form method="post" action="/info" id="form">#}
		{#<p>Выберите курс:#}
			{#<select name="course" id="course" onmousedown="$(':first-child', this).remove(); this.onmousedown = null;">#}
				{#<option value=""></option>#}
				{#{% for c in courses %}#}
					{#<option>{{ c.course }}</option>#}
				{#{% endfor %}#}
			{#</select>#}
			{#<span id="resp1"></span>#}

	{#</form>#}
{#{% endif %}#}
{#{% if (flag2===true) %}#}
	{#<div class="table2">#}
		{#<h1>Результат:</h1>#}
		{#<p>Данные о оценках за {{ sem[0].semester }} семестр {{ kur[0].course }}-го курса </p>#}
		{#<table border="1">#}
			{#<thead>#}
				{#<tr>#}
					{#<th>Предмет</th>#}
					{#<th>Средняя оценка</th>#}
					{#<th>Пропуски</th>#}
				{#</tr>#}
			{#</thead>#}
			{#<tbody>#}
				{#{% for p in progr %}#}
					{#{% set sub = subj[p.subject_id] %}#}
					{#<tr>#}
						{#<td>{{ sub.name }}</td>#}
						{#<td>{{ p.grade }}</td>#}
						{#<td>{{ p.omission }}</td>#}
					{#</tr>#}
				{#{% endfor %}#}
			{#</tbody>#}
		{#</table>#}
	{#</div>#}

{#{% endif %}#}
{#{% if (flag===true) %}#}
	{#<div class="table">#}
		{#<h1>Результат:</h1>#}
		{#<p>Данные о оценках за {{ sem }} семестр {{ kur }}-го курса </p>#}
		{#<table border="1">#}
			{#<thead>#}
				{#<tr>#}
					{#<th>Предмет</th>#}
					{#<th>Средняя оценка</th>#}
					{#<th>Пропуски</th>#}
				{#</tr>#}
			{#</thead>#}
			{#<tbody>#}
				{#{% for p in progress %}#}
					{#{% set sub = subjects[p.subject_id] %}#}
					{#<tr>#}
						{#<td>{{ sub.name }}</td>#}
						{#<td>{{ p.grade }}</td>#}
						{#<td>{{ p.omission }}</td>#}
					{#</tr>#}
				{#{% endfor %}#}
			{#</tbody>#}
		{#</table>#}
	{#</div>#}

{#{% endif %}#}

{#{% if session.get('authorization') === 1 %}#}
	{#<a href="/groups">Группы</a>#}
	{#<a href="/subjects">Предметы</a>#}
	{#<a href="/students">Ученики</a>#}
	{#<h2>Основное</h2>#}


	{#<form method="post" id="form2" action="/progress">#}
		{#<div id="resp1"> Выберите группу:#}
			{#<select name="group" id="group" onmousedown="$(':first-child', this).remove(); this.onmousedown = null;">#}
				{#<option value=0></option>#}
				{#{% for g in group %}#}
					{#<option>{{ g.name }}</option>#}
				{#{% endfor %}#}
			{#</select>#}
			{#<input name="year" id="year" type="number" min="1900" max="2099" step="1" value="2017"/>#}
			{#<input type="button" id="b1" value="ok">#}
		{#</div>#}
		{#<div id="resp2">#}

		{#</div>#}
		{#<div id="resp3">#}

		{#</div>#}
	{#</form>#}
	{#<div id="response"></div>#}
	{#<a href="/progress/create">Создать прогресс</a>#}
{#{% endif %}#}


{#<p><a href="/setting">ИЗМЕНИТЬ ДАННЫЕ</a></p>#}


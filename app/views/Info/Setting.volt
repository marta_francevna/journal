<header class="header">
    <div class="b-title-1">
        <span class="text">Электронный дневник</span>
    </div>
    <div class="b-title-2">
        <span class="text">Редактирование профиля</span>
    </div>
</header>

<div class="main">
    <form method="post" action="/setting" id="loginform" class="form-entry">
        <span id="resp1" class="errors" >
            {% for message in form.getMessages() %}
	        <p>{{ message.getMessage() }}</p>
	        {% endfor %}
        </span>
        <div class="group">
            <input type="text" name="login" id="login" value="{{ user.login }}" required>
            <span class="bar"></span>
            <label>Логин</label>
        </div>
        <div class="group">
            <input type="password" name="password" id="password" required>
            <span class="bar"></span>
            <label>Пароль</label>
        </div>
        <div class="b-button button-custom">
            <a href="/info" class="link-back">« Назад</a>
            <button type="submit" class="button">Сохранить</button>
        </div>
    </form>
</div>


{#<h1>Редактирование информации о пользователе</h1>#}
{#<form action="/setting" method="post">#}

{#<label>login</label>#}
    {#{{ form.render('login') }}#}
{#<label>Пароль</label>#}
    {#<input type="password" name="password">#}
    {#<button type="submit">Сохранить</button>#}
{#</form>#}

{#<div id="errors">#}
    {#<ul>#}
        {#{% for message in form.getMessages() %}#}
            {#<li>{{ message.getMessage() }}</li>#}
        {#{% endfor %}#}
    {#</ul>#}
{#</div>#}
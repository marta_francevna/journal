<header class="header">
	<div class="b-title-1">
		<span class="text">Электронный дневник</span>
	</div>
	<div class="b-title-2">
		<span class="text">Основное</span>
	</div>
</header>
<nav class="navbar">
	<div class="b-navbar container">
		<ul class="menu-items">
			<li><a href="/info" class="active">Основное</a></li>
			<li><a href="/subjects">Предметы</a></li>
			<li><a href="/students">Ученики</a></li>
			<li><a href="/groups">Группы</a></li>
		</ul>
		<div class="b-login">
			<a href="/setting" class="login"> {{ session.get('login')}}</a>
			<a href="/logout" class="exit">Выход</a>
		</div>
	</div>
</nav>

<div class="main">
	<div class="b-main">
		<div class="b-text">
			<span class="text">Группа: {{ group }}. Курс: {{ kur }}. Семестр: {{ sem }}</span>
		</div>
		<table>
			<thead>
				<tr>
					<th>ФИО</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				{% for p in progress %}
				{% set student = names[p.user_id] %}
				<tr>
					<td>{{ student.name }}</td>
					<td><a href="/student/{{ p.user_id }}/course/{{ kur }}/semester/{{ sem }}" class="book"></a></td>
				</tr>
				{% endfor %}
			</tbody>
		</table>
	</div>
</div>

{#<div class="table">#}
	{#<h1>Результат:</h1>#}
	{#<p>Ученики группы {{ group }} за {{ sem }} семестр {{ kur }}-го курса </p>#}
	{#<table border="1">#}
		{#<thead>#}
			{#<tr>#}
				{#<th>Имя</th>#}
				{#<th></th>#}
			{#</tr>#}
		{#</thead>#}
		{#<tbody>#}
			{#{% for p in progress %}#}
				{#{% set student = names[p.user_id] %}#}
				{#<tr>#}
					{#<td>{{ student.name }}</td>#}
					{#<td><a href="/student/{{ p.user_id }}/course/{{ kur }}/semester/{{ sem }}">посмотреть</a></td>#}
				{#</tr>#}
			{#{% endfor %}#}
		{#</tbody>#}
	{#</table>#}
{#</div>#}
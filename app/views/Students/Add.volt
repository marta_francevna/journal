
<header class="header">
	<div class="b-title-1">
		<span class="text">Электронный дневник</span>
	</div>
	<div class="b-title-2">
		<span class="text">Добавление ученика</span>
	</div>
</header>

<div class="main">
	<form method="post" action="/students/add"  class="form-edit-inf">
		<label>ФИО:</label>
		{{ form.render('name') }}
		<label>Группа</label>
		{{ form.render('group_id') }}
		<label>Логин</label>
		{{ form.render('login') }}
		<label>Пароль</label>
		{{ form.render('password') }}
		<label>Студенческий</label>
		{{ form.render('number') }}
		<input type="hidden" name="role" value=0>
		<label>Статус</label>
		{{ form.render('status') }}
		<div class="b-button button-custom button-edit-inf">
			<a href="/students" class="link-back">« Назад</a>
			<button type="submit" class="button">Создать</button>
		</div>
		<span class="errors">
			{% for message in form.getMessages() %}
				<p>{{ message.getMessage() }}</p>
			{% endfor %}
		</span>
	</form>
</div>

{#<h1>Добавление ученика</h1>#}
{#<form action="/students/add" method="post">#}
	{#<label>ФИО:</label>#}
	{#{{ form.render('name') }}#}
	{#<label>Группа</label>#}
	{#{{ form.render('group_id') }}#}
	{#<label>Логин</label>#}
	{#{{ form.render('login') }}#}
	{#<label>Пароль</label>#}
	{#{{ form.render('password') }}#}
	{#<label>Студенческий</label>#}
	{#{{ form.render('number') }}#}
	{#<input type="hidden" name="role" value=0>#}
	{#<label>Статус</label>#}
	{#{{ form.render('status') }}#}
	{#<input type="submit" value="Создать">#}
{#</form>#}
{#<div id="errors">#}
	{#{% for message in form.getMessages() %}#}
		{#<p>{{ message.getMessage() }}</p>#}
	{#{% endfor %}#}
{#</div>#}
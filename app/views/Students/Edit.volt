
<header class="header">
	<div class="b-title-1">
		<span class="text">Электронный дневник</span>
	</div>
	<div class="b-title-2">
		<span class="text">Редактирование ученика</span>
	</div>
</header>

<div class="main">
	<form method="post" action="/students/edit/{{ student.getId() }}"  class="form-edit-inf">
		<label>ФИО:</label>
		{{ form.render('name') }}
		<label>Группа</label>
		{{ form.render('group_id') }}
		<label>Логин</label>
		{{ form.render('login') }}
		<label>Пароль</label>
		{{ form.render('password') }}
		<label>Студенческий</label>
		{{ form.render('number') }}
		{{ form.render('role') }}
		<label>Статус</label>
		{{ form.render('status') }}
		<div class="b-button button-custom button-edit-inf">
			<a href="/students" class="link-back">« Назад</a>
			<button type="submit" class="button">Сохранить</button>
		</div>
		<span class="errors">
			{% for message in form.getMessages() %}
				<p>{{ message.getMessage() }}</p>
			{% endfor %}
		</span>
	</form>
</div>


{#<h1>Редактирование ученика</h1>#}
{#<form action="/students/edit/{{ student.getId() }}" method="post">#}
	{#<label>ФИО:</label>#}
	{#{{ form.render('name') }}#}
	{#<label>Группа</label>#}
	{#{{ form.render('group_id') }}#}
	{#<label>Логин</label>#}
	{#{{ form.render('login') }}#}
	{#<label>Пароль</label>#}
	{#{{ form.render('password') }}#}
	{#<label>Студенческий</label>#}
	{#{{ form.render('number') }}#}
	{#{{ form.render('role') }}#}
	{#<label>Статус</label>#}
	{#{{ form.render('status') }}#}
	{#<input type="submit" value="Сохранить">#}
{#</form>#}
{#<div id="errors">#}
	{#{% for message in form.getMessages() %}#}
		{#<p>{{ message.getMessage() }}</p>#}
	{#{% endfor %}#}
{#</div>#}
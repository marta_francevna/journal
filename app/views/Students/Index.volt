<header class="header">
	<div class="b-title-1">
		<span class="text">Электронный дневник</span>
	</div>
	<div class="b-title-2">
		<span class="text">Ученики</span>
	</div>
</header>
<nav class="navbar">
	<div class="b-navbar container">
		<ul class="menu-items">
			<li><a href="/info">Основное</a></li>
			<li><a href="/subjects">Предметы</a></li>
			<li><a href="/students" class="active">Ученики</a></li>
			<li><a href="/groups">Группы</a></li>
		</ul>
		<div class="b-login">
			<a href="/setting" class="login"> {{ session.get('login') }}</a>
			<a href="/logout" class="exit">Выход</a>
		</div>
	</div>
</nav>
<div class="main">
	<div class="b-main">

		<form class="search-form" method="post" action="/students">
			<input class="search-input" type="text" name="search" placeholder="">
			<button class="search-button" name="submit"></button>
		</form>
		{% if page.items|length>0 %}
		<table>
			<thead>
				<tr>
					<th>ФИО</th>
					<th>Группа</th>
					<th>Студенческий</th>
					<th>Статус</th>
					<th>Изм.</th>
					<th>Уд.</th>
				</tr>
			</thead>
			<tbody>
				{% for item in page.items %}
				{% set group = groups[item.group_id] %}
				<tr>
					<td>{{ item.name }}</td>
					<td>{{ group.name }}</td>
					<td>{{ item.number }}</td>
					{% if item.status == 1 %}
					<td>Отчислен</td>
					{% else %}
					<td>Учится</td>
					{% endif %}
					<td><a href="/students/edit/{{ item.getId() }}" class="edit"></a></td>
					<td><a href="/students/del/{{ item.getId() }}" class="delete"></a></td>
				</tr>
				{% endfor %}
			</tbody>
		</table>
		<a href="/students/add">Добавить</a>
		{% if page.total_pages > 1 %}
			<div class="numbers">
				<a class="prev page-numbers" href="/students/page/{{ page.before }}">«</a>
				{% set reserve =  3 %}
				{% set count =  7 %}


				{% set startP = 1 %}
				{% set countP = (page.last - (count + startP))>0 ? (count + startP) : page.last %}

				{% if   page.current - reserve > 0 %}

					{% set startP = page.current - reserve %}
					{% set countP = (page.last - (count + startP))>0 ? count + startP : page.last %}

				{% endif %}

				{% if countP == 0 %}
					{% set countP = 1 %}
				{% endif %}
				{% if   page.current > reserve + 1 %}
					<a class="page-numbers" href="/students/page/1">1</a>
					<span aria-hidden="true">...</span>
				{% endif %}
				{#body#}
				{% for i in startP..countP %}
					<a {{ (i==page.current)?'class="page-numbers current"':'page-numbers"' }} class="page-numbers" href="/students/page/{{ i }}">{{ i }}</a>
				{% endfor %}
				{#footer#}
				{% if   page.last - startP > count %}
					<span aria-hidden="true">...</span>
					<a {{ (i==page.current)?'class="page-numbers current"':'page-numbers"' }} class="page-numbers" href="/students/page/{{ page.last }}">{{ page.last }}</a>
				{% endif %}

				<a class="next page-numbers" href="/students/page/{{ page.next }}">»</a>
			</div>
		{% endif %}
	</div>

</div>
{% else %}
	<p>Извините ничего не найдено:(</p>
{% endif %}

{#<form method="post" action="/students">#}
	{#<input type="text" name="search">#}
	{#<input type="submit" value="найти">#}
{#</form>#}
{#<h1>Студенты</h1>#}
{#{% if page.items|length>0 %}#}
{#<table border="1">#}
	{#<thead>#}
		{#<tr>#}
			{#<th>ФИО</th>#}
			{#<th>Группа</th>#}
			{#<th>Студенческий</th>#}
			{#<th>Статус</th>#}
			{#<th>Редактировать</th>#}
			{#<th>Удалить</th>#}
		{#</tr>#}
	{#</thead>#}
	{#<tbody>#}
		{#{% for item in page.items %}#}
			{#{% set group = groups[item.group_id] %}#}
			{#<tr>#}
				{#<td>{{ item.name }}</td>#}
				{#<td>{{ group.name }}</td>#}
				{#<td>{{ item.number }}</td>#}
				{#{% if item.status == 1 %}#}
					{#<td>Отчислен</td>#}
				{#{% else %}#}
					{#<td>Учится</td>#}
				{#{% endif %}#}
				{#<td><a href="/students/edit/{{ item.getId() }}">Редактировать</a></td>#}
				{#<td><a href="/students/del/{{ item.getId() }}">Удалить</a></td>#}
			{#</tr>#}
		{#{% endfor %}#}
	{#</tbody>#}
{#</table>#}
{#{% else %}#}
	{#<p>Извините ничего не найдено:(</p>#}
{#{% endif %}#}
{#<a href="/students/add">Добавить</a>#}
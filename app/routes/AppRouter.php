<?php

class AppRouter extends \Phalcon\Mvc\Router\Group
{
	public function initialize()
	{
		$this->setPaths([
			'controller' => 'Index',
			'action'     => 'Index',
		]);

		$this->add('');

		$this->add('/');

		$this->add('/:controller', [
			'controller' => 1,
		])->convert('controller', function ($controller) {
			return \Phalcon\Text::camelize($controller);
		});

		$this->add('/:controller/:action', [
			'controller' => 1,
			'action'     => 2,
		])->convert('controller', function ($controller) {
			return \Phalcon\Text::camelize($controller);
		})->convert('action', function ($action) {
			return \Phalcon\Text::camelize($action);
		});

		$this->add('/:controller/:action/:params', [
			'controller' => 1,
			'action'     => 2,
			'params'     => 3,
		])->convert('controller', function ($controller) {
			return \Phalcon\Text::camelize($controller);
		})->convert('action', function ($action) {
			return \Phalcon\Text::camelize($action);
		});

		$this->add("/login", [
			'controller' => 'Index',
			'action'     => 'Login',
		]);

		$this->add("/logout", [
			'controller' => 'Index',
			'action'     => 'Logout',
		]);

		$this->add("/setting", [
			'controller' => 'Info',
			'action'     => 'Setting',
		]);

		$this->add("/course", [
			'controller' => 'Info',
			'action'     => 'Course',
		]);
		$this->add("/semesters", [
			'controller' => 'Info',
			'action'     => 'Semesters',
		]);
		$this->add("/progress", [
			'controller' => 'Info',
			'action'     => 'Progress',
		]);

		$this->add("/student/([0-9]+)/course/([0-9]+)/semester/([0-9]+)", [
			'controller' => 'Info',
			'action'     => 'Students',
			'user_id'    => 1,
			'course'     => 2,
			'semester'   => 3,
		]);

		$this->add("/progress/del/([0-9]+)", [
			'controller' => 'Progress',
			'action'     => 'Del',
			'id'         => 1,
		]);

		$this->add("/progress/edit/([0-9]+)", [
			'controller' => 'Progress',
			'action'     => 'Edit',
			'id'         => 1,
		]);

		$this->add("/progress/add/", [
			'controller' => 'Progress',
			'action'     => 'Add',
		]);

		$this->add("/students/del/([0-9]+)", [
			'controller' => 'Students',
			'action'     => 'Del',
			'id'         => 1,
		]);

		$this->add("/students/edit/([0-9]+)", [
			'controller' => 'Students',
			'action'     => 'Edit',
			'id'         => 1,
		]);

		$this->add("/students/add/", [
			'controller' => 'Students',
			'action'     => 'Add',
		]);

		$this->add("/subjects/del/([0-9]+)", [
			'controller' => 'Subjects',
			'action'     => 'Del',
			'id'         => 1,
		]);

		$this->add("/subjects/edit/([0-9]+)", [
			'controller' => 'Subjects',
			'action'     => 'Edit',
			'id'         => 1,
		]);

		$this->add("/subjects/add/", [
			'controller' => 'Subjects',
			'action'     => 'Add',
		]);

		$this->add("/groups/del/([0-9]+)", [
			'controller' => 'Groups',
			'action'     => 'Del',
			'id'         => 1,
		]);

		$this->add("/groups/edit/([0-9]+)", [
			'controller' => 'Groups',
			'action'     => 'Edit',
			'id'         => 1,
		]);

		$this->add("/groups/add/", [
			'controller' => 'Groups',
			'action'     => 'Add',
		]);

		$this->add('/groups/page/([0-9]+)',[
			'controller' => 'Groups',
			'action'     => 'Index',
			'page'       => 1,
		]);

		$this->add('/students/page/([0-9]+)',[
			'controller' => 'Students',
			'action'     => 'Index',
			'page'       => 1,
		]);

		$this->add('/subjects/page/([0-9]+)',[
			'controller' => 'Subjects',
			'action'     => 'Index',
			'page'       => 1,
		]);

		$this->add("/progress/create/", [
			'controller' => 'Progress',
			'action'     => 'Create',
		]);

		$this->add("/progress/select/", [
			'controller' => 'Progress',
			'action'     => 'Select',
		]);





	}

}
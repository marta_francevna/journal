<?php

use Phalcon\Mvc\View;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class GroupsController extends ControllerBase
{

	public function indexAction()
	{
		$search = $this->request->getPost("search");

		if (!$search){
			$currentPage = $this->dispatcher->getParam('page');
			$groups      = Group::find();
			$paginator   = new PaginatorModel(
				[
					"data"  => $groups,
					"limit" => 1,
					"page"  => $currentPage,
				]
			);

			$page = $paginator->getPaginate();

			$this->view->setVars([
				'page' => $page,
			]);

		}else{
			$currentPage = $this->dispatcher->getParam('page');
			$groups = Group::find([
				'conditions' => 'name like :search: or year like :search: ',
				'bind' => [
					'search' => '%' . $search .'%',
				]]);
			$paginator = new PaginatorModel(
				[
					"data"  => $groups,
					"limit" => 10,
					"page"  => $currentPage,
				]
			);
			$page = $paginator->getPaginate();
			$this->view->setVar('page' , $page);

			$this->session->set("search", $search);
		}


	}

	public function addAction()
	{

		$form  = new GroupsForm();
		$group = new Group();

		$this->view->setVars([
			'form'  => $form,
			'group' => $group,
		]);

		if ( !$this->request->isPost() ) {
			return $this->view;
		}

		if ( $form->isValid($this->request->getPost(), $group) ) {

			if ( $group->create() ) {
				return $this->response->redirect('/groups', true);
			}
		}
	}

	public function editAction()
	{
		$id = $this->dispatcher->getParam('id');

		$group = Group::findFirst([
			'conditions' => 'id = :id: ',
			'bind'       => [
				'id' => $id,
			]]);

		$form = new GroupsForm($group);
		$this->view->setVars([
			'form'  => $form,
			'group' => $group,
		]);
		if ( !$this->request->isPost() ) {
			return $this->view;
		}
		if ( $form->isValid($this->request->getPost(), $group) ) {

			if ( $group->save() ) {
				return $this->response->redirect('/groups', true);
			}
		}
	}

	public function delAction()
	{
		$id    = $this->dispatcher->getParam('id');
		$group = Group::findFirst([
			'conditions' => 'id = :id: ',
			'bind'       => [
				'id' => $id,
			]]);
		$group->delete();

		return $this->response->redirect('/groups', true);
	}

}


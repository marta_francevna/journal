<?php

class InfoController extends ControllerBase
{
	public function initialize()
	{
		$avrz = $this->session->get("authorization");
		if ( $avrz === null ) {

			$this->dispatcher->forward([
				'controller' => 'Index',
				'action'     => 'Route404',
			]);
		}
	}

	public function indexAction()
	{

		$login = $this->session->get("login");
		$user  = Users::findFirst([
			'conditions' => 'login = :login: or number = :login:',
			'bind'       => [
				'login' => $login,
			]]);

		$this->view->setVar('user', $user);
		$avrz  = $this->session->get("authorization");
		$flag  = false;
		$flag2 = true;
		if ( $avrz === 0 ) {
			$courses = Progress::query()
				->columns("DISTINCT course")
				->where("group_id = :group_id:")
				->bind(['group_id' => $user->getGroupId()])
				->orderBy('course DESC')
				->execute();

			$kur = Progress::query()
				->columns("DISTINCT course")
				->where("group_id = :group_id:")
				->bind(['group_id' => $user->getGroupId()])
				->orderBy('course DESC')
				->execute();

			$sem = Progress::query()
				->columns("DISTINCT semester")
				->where("group_id = :group_id:")
				->andWhere('course = :course:')
				->bind([
					'group_id' => $user->getGroupId(),
					'course'   => $kur[0]->course,
				])
				->orderBy('semester DESC')
				->execute();

			$progr    = Progress::find([
				'conditions' => 'user_id = :user_id: and course = :course: and semester = :semester: and group_id = :group_id:',
				'bind'       => [
					'user_id'  => $user->getId(),
					'course'   => $kur[0]->course,
					'semester' => $sem[0]->semester,
					'group_id' => $user->getGroupId(),
				]]);
			$Subjects = Subject::find();
			$temp     = [];
			foreach ( $Subjects as $sub ) {
				$temp[$sub->getId()] = $sub;
			}

			$this->view->setVars([
				'courses' => $courses,
				'flag'    => $flag,
				'flag2'   => $flag2,
				'sem'     => $sem,
				'kur'     => $kur,
				'subj'    => $temp,
				'progr'   => $progr,
			]);
			if ( !$this->request->isPost() ) {
				return $this->view;
			}
			$semester = $this->request->getPost('semester');
			$course   = $this->request->getPost('course');
			if ( $semester == null ) {
				$semesters = Progress::query()
					->columns("DISTINCT semester")
					->where("group_id = :group_id:")
					->andWhere('course = :course:')
					->bind([
						'group_id' => $user->getGroupId(),
						'course'   => $course,
					])
					->execute();

				return $this->JsonResponse([$semesters]);
			}

			$group_id = $user->getGroupId();
			if ( $this->request->isPost() ) {
				$flag     = true;
				$flag2    = false;
				$progress = Progress::find([
					'conditions' => 'user_id = :user_id: and course = :course: and semester = :semester: and group_id = :group_id:',
					'bind'       => [
						'user_id'  => $user->getId(),
						'course'   => $course,
						'semester' => $semester,
						'group_id' => $group_id,
					]]);
				$Subjects = Subject::find();
				$temp     = [];
				foreach ( $Subjects as $sub ) {
					$temp[$sub->getId()] = $sub;
				}

				$this->view->setVars([
					'progress' => $progress,
					'subjects' => $temp,
					'flag'     => $flag,
					'flag2'    => $flag2,
					'sem'      => $semester,
					'kur'      => $course,
				]);
			}
		} else {
			$flag2 = false;
			$group = Group::query()
				->columns("name, id")
				->distinct("name")
				->execute();
			$this->view->setVars([
				'flag'  => $flag,
				'flag2' => $flag2,
				'group' => $group,
			]);
		}
	}

	public function settingAction()
	{

		$login = $this->session->get("login");

		$user = Users::findFirst([
			'conditions' => 'login = :login: or number = :login:',
			'bind'       => [
				'login' => $login,
			]]);
		$form = new UsersForm($user);

		$this->view->setVars([
			'form' => $form,
			'user' => $user,
		]);
		if ( !$this->request->isPost() ) {
			return $this->view;
		}
		if ( $form->isValid($this->request->getPost(), $user) ) {

			if ( $user->save() ) {
				$login = $this->request->getPost("login");
				$this->session->set("login", $login);

				return $this->response->redirect('/info');
			}
		}
	}

	public function courseAction()
	{

		$group_name = $this->request->getPost('group');
		$year       = $this->request->getPost('year');
		$group      = Group::findFirst([
			'conditions' => 'year = :year: and name = :name:',
			'bind'       => [
				'year' => $year,
				'name' => $group_name,
			]]);

		if ( $group ) {
			$course = Progress::query()
				->columns("DISTINCT course")
				->where("group_id = :group_id:")
				->bind(['group_id' => $group->getId()])
				->orderBy('course DESC')
				->execute();

			$this->session->set('group_id', $group->getId());
			return $this->JsonResponse([$course]);
		} else {
			return $this->JsonResponse([0]);
		}
	}

	public function semestersAction()
	{

		$course     = $this->request->getPost('course');
		$group_name = $this->request->getPost('group');
		$year       = $this->request->getPost('year');
		$group      = Group::findFirst([
			'conditions' => 'year = :year: and name = :name:',
			'bind'       => [
				'year' => $year,
				'name' => $group_name,
			]]);

		$semesters = Progress::query()
			->columns("DISTINCT semester")
			->where("group_id = :group_id:")
			->andWhere('course = :course:')
			->bind([
				'group_id' => $group->getId(),
				'course'   => $course,
			])
			->orderBy('semester DESC')
			->execute();

		return $this->JsonResponse([$semesters]);
	}

	public function progressAction()
	{
		$course     = $this->request->getPost('cur');
		$group_name = $this->request->getPost('group');
		$year       = $this->request->getPost('year');
		$semester   = $this->request->getPost('sem');

		if ( !$this->request->isPost() ) {

			 $this->dispatcher->forward([
				'controller' => 'Index',
				'action'     => 'Route404',
			]);
		}
		else {
			$group = Group::findFirst([
				'conditions' => 'year = :year: and name = :name:',
				'bind'       => [
					'year' => $year,
					'name' => $group_name,
				]]);

			$progress = Progress::query()
				->columns("DISTINCT user_id")
				->where("group_id = :group_id:")
				->andWhere('course = :course:')
				->andWhere('semester = :semester:')
				->bind([
					'group_id' => $group->getId(),
					'course'   => $course,
					'semester' => $semester,
				])
				->execute();
			$group_id = $group->getId();
			$this->session->set("group_id", $group_id);
			$names = Users::find();
			$temp  = [];
			foreach ( $names as $name ) {
				$temp[$name->getId()] = $name;
			}

			$this->view->setVars([
				'progress' => $progress,
				'group'    => $group_name,
				'sem'      => $semester,
				'kur'      => $course,
				'names'    => $temp,
			]);
		}
		}

	public function studentsAction(){

		$user_id = $this->dispatcher->getParam('user_id');
		$this->session->set("user_id", $user_id);
		$course = $this->dispatcher->getParam('course');
		$this->session->set("course", $course);
		$semester = $this->dispatcher->getParam('semester');
		$this->session->set("semester", $semester);
		$user = Users::findFirst($user_id);
		if ($user){
			$progress = Progress::find([
				'conditions' => 'user_id = :user_id: and course = :course: and semester = :semester: and group_id = :group_id:',
				'bind'       => [
					'user_id'  => $user->getId(),
					'course'   => $course,
					'semester' => $semester,
					'group_id' => $user->getGroupId(),
				]]);
			$a = count($progress);
			if ($a>0)
			{

				$Subjects = Subject::find();
				$temp     = [];
				foreach ( $Subjects as $sub ) {
					$temp[$sub->getId()] = $sub;
				}

				$this->view->setVars([
					'progress' => $progress,
					'subjects' => $temp,
					'student'   =>$user->getName()
				]);
			}
			if($a==0){
				$this->response->redirect("/",TRUE);
			}
		}else{
			$this->dispatcher->forward([
				'controller' => 'Index',
				'action'     => 'Route404',
			]);
		}



	}

}


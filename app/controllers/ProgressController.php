<?php

use Phalcon\Mvc\View;

class ProgressController extends ControllerBase
{

	public function indexAction()
	{
	}

	public function addAction()
	{

		$form     = new ProgressForm();
		$progress = new Progress();

		$this->view->setVars([
			'form'     => $form,
			'progress' => $progress,
		]);

		if ( !$this->request->isPost() ) {
			return $this->view;
		}

		if ( $form->isValid($this->request->getPost(), $progress) ) {

			if ( $progress->create() ) {
				$user     = $this->request->getPost('user_id');
				$course   = $this->request->getPost('course');
				$semester = $this->request->getPost('semester');
				$url      = ('/student/' . $user . '/course/' . $course . '/semester/' . $semester);

				return $this->response->redirect($url, true);
			}
		}
	}

	public function editAction()
	{
		$id = $this->dispatcher->getParam('id');

		$progress = Progress::findFirst([
			'conditions' => 'id = :id: ',
			'bind'       => [
				'id' => $id,
			]]);

		$form = new ProgressForm($progress);
		$this->view->setVars([
			'form'     => $form,
			'progress' => $progress,
		]);
		if ( !$this->request->isPost() ) {
			return $this->view;
		}
		if ( $form->isValid($this->request->getPost(), $progress) ) {

			if ( $progress->save() ) {
				$user     = $this->request->getPost('user_id');
				$course   = $this->request->getPost('course');
				$semester = $this->request->getPost('semester');
				$url      = ('/student/' . $user . '/course/' . $course . '/semester/' . $semester);

				return $this->response->redirect($url, true);
			}
		}
	}

	public function delAction()
	{
		$id       = $this->dispatcher->getParam('id');
		$progress = Progress::findFirst([
			'conditions' => 'id = :id: ',
			'bind'       => [
				'id' => $id,
			]]);
		$progress->delete();
		$url = $this->request->getPost('url');

		return $this->response->redirect($url, true);
	}

	public function createAction(){

		$group = Group::query()
			->columns("name, id")
			->distinct("name")
			->execute();
		$this->view->setVars([
			'group' => $group,
			]);

		$form     = new ProgressCreateForm();
		$progress = new Progress();

		$this->view->setVars([
			'form'     => $form,
			'progress' => $progress,
		]);

		if ( !$this->request->isPost() ) {
			return $this->view;
		}
		$user_id = $this->request->getPost('user_id');
		if ( $form->isValid($this->request->getPost(), $progress) ) {

			if ( $progress->create() ) {
				return $this->response->redirect('/info', true);
			}
		}
	}


	public function selectAction(){

		$group_name = $this->request->getPost('group');
		$year       = $this->request->getPost('year');
		$group      = Group::findFirst([
			'conditions' => 'year = :year: and name = :name:',
			'bind'       => [
				'year' => $year,
				'name' => $group_name,
			]]);

		if ( $group ) {
			$users = Users::query()
				->columns(["id","name"])
				->distinct("name")
				->where("group_id = :group_id:")
				->bind(['group_id' => $group->getId()])
				->execute();

			return $this->JsonResponse([$users]);
		} else {
			return $this->JsonResponse([0]);
		}
	}

}


<?php

use Phalcon\Mvc\View;
class ErrorController extends ControllerBase
{

	public function show404Action()
	{
		$this->response->setStatusCode(404, 'Not Found');
		$this->view->pick('404/404');

		$this->view->setRenderLevel(
			View::LEVEL_ACTION_VIEW
		);

	}


}


<?php

use Phalcon\Mvc\View;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class StudentsController extends ControllerBase
{

	public function indexAction()
	{
		$search = $this->request->getPost("search");
		if (!$search) {
			$currentPage = $this->dispatcher->getParam('page');
			$students    = Users::find([
				'conditions' => 'role = :role: ',
				'bind'       => [
					'role' => 0,
				]]);

			$paginator = new PaginatorModel(
				[
					"data"  => $students,
					"limit" => 1,
					"page"  => $currentPage,
				]
			);
			$Groups    = Group::find();
			$temp      = [];
			foreach ( $Groups as $group ) {
				$temp[$group->getId()] = $group;
			}

			$page = $paginator->getPaginate();

			$this->view->setVars([
				'groups' => $temp,
				'page'   => $page,
			]);
		}else{
			$currentPage = $this->dispatcher->getParam('page');
			$students = Users::find([
				'conditions' => '(name like :search: or number like :search: ) and role = :role: ',
				'bind' => [
					'search' => '%' . $search .'%',
					'role' => 0,
				]]);
			$paginator = new PaginatorModel(
				[
					"data"  => $students,
					"limit" => 10,
					"page"  => $currentPage,
				]
			);
			$Groups    = Group::find();
			$temp      = [];
			foreach ( $Groups as $group ) {
				$temp[$group->getId()] = $group;
			}

			$page = $paginator->getPaginate();
			$this->view->setVars([
				'groups' => $temp,
				'page'   => $page,
			]);
			$this->session->set("search", $search);
		}

	}

	public function addAction()
	{

		$form     = new UserForm();
		$student = new Users();

		$this->view->setVars([
			'form'     => $form,
			'student' => $student,
		]);

		if ( !$this->request->isPost() ) {
			return $this->view;
		}

		if ( $form->isValid($this->request->getPost(), $student) ) {

			if ( $student->create() ) {
				return $this->response->redirect('/students', true);
			}
		}
	}

	public function editAction()
	{
		$id = $this->dispatcher->getParam('id');

		$student = Users::findFirst([
			'conditions' => 'id = :id: ',
			'bind'       => [
				'id' => $id,
			]]);

		$form = new UserForm($student);
		$this->view->setVars([
			'form'     => $form,
			'student' => $student,
		]);
		if ( !$this->request->isPost() ) {
			return $this->view;
		}
		if ( $form->isValid($this->request->getPost(), $student) ) {

			if ( $student->save() ) {
				return $this->response->redirect('/students', true);
			}
		}
	}

	public function delAction()
	{
		$id       = $this->dispatcher->getParam('id');
		$student = Users::findFirst([
			'conditions' => 'id = :id: ',
			'bind'       => [
				'id' => $id,
			]]);
		$student->delete();
		return $this->response->redirect('/students', true);
	}

}


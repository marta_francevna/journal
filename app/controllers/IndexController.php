<?php

use Phalcon\Mvc\View;
class IndexController extends ControllerBase
{

    public function indexAction()
    {
        if ($this->session->has("authorization")){
            $this->response->redirect("/info",TRUE);
        }
    }

    public function loginAction(){
        $form = new LoginForm();
        $users = new Users();
        $this->view->setVars([
            'form' => $form,
            'users' => $users,
        ]);
        if (!$this->request->isPost()) {
            return $this->view;
        }
        $login = $this->request->getPost("login");
        $pass = $this->request->getPost("password");
        if ($login=="" || $pass=="" ){
            return $this->JsonResponse([2]);
        }
        $form->bind($_POST, $users);
        if ($form->isValid()) {

            $user = Users::findFirst([
                'conditions' => 'login = :login: and password = :password: and role = 1',
                'bind' => [
                    'login' => $login,
                    'password' => $pass,
                ]]);
            if ($user) {
                $authorization = 1;
                $this->session->set("login", $login);
                $this->session->set("authorization", $authorization);
                return $this->JsonResponse([3]);
            }
            $user = Users::findFirst([
                'conditions' => '(login = :login: or number = :login:) and password = :password: and role = 0',
                'bind' => [
                    'login' => $login,
                    'password' => $pass,
                ]]);
            if ($user) {
                $authorization = 0;
                $this->session->set("login", $login);
                $this->session->set("authorization", $authorization);
                $this->JsonResponse([3]);

                return $this->JsonResponse([3]);
            }
            else{
                return $this->JsonResponse([1]);
            }
        }else{
            return $this->JsonResponse([0]);
        }
    }

    public function LogoutAction(){

        $this->session->remove("authorization");
        $this->session->remove("login");
        $this->response->redirect("/",TRUE);
	    $this->session->destroy();
	    //TODO:удаление всех сессий

    }

    public function Route404Action()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        //TODO:404
    }


}


<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    /**
     * @param array $data
     *
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    protected function JsonResponse(array $data)
    {
        return $this->response->setJsonContent($data);
    }


}

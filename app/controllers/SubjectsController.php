<?php

use Phalcon\Mvc\View;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class SubjectsController extends ControllerBase
{

	public function indexAction()
	{
		$search = $this->request->getPost("search");
		if (!$search) {
			$currentPage = $this->dispatcher->getParam('page');
			$subjects    = Subject::find();
			$paginator   = new PaginatorModel(
				[
					"data"  => $subjects,
					"limit" => 1,
					"page"  => $currentPage,
				]
			);

			$page = $paginator->getPaginate();

			$this->view->setVars([
				'page' => $page,
			]);
		}else{
			$currentPage = $this->dispatcher->getParam('page');
			$subjects = Subject::find([
				'conditions' => 'name like :search: ',
				'bind' => [
					'search' => '%' . $search .'%',
				]]);
			$paginator = new PaginatorModel(
				[
					"data"  => $subjects,
					"limit" => 10,
					"page"  => $currentPage,
				]
			);
			$page = $paginator->getPaginate();
			$this->view->setVar('page' , $page);
			$this->session->set("search", $search);
		}
	}

	public function addAction()
	{

		$form     = new SubjectsForm();
		$subjects = new Subject();

		$this->view->setVars([
			'form'     => $form,
			'subjects' => $subjects,
		]);

		if ( !$this->request->isPost() ) {
			return $this->view;
		}

		if ( $form->isValid($this->request->getPost(), $subjects) ) {

			if ( $subjects->create() ) {
				return $this->response->redirect('/subjects', true);
			}
		}
	}

	public function editAction()
	{
		$id = $this->dispatcher->getParam('id');

		$subjects = Subject::findFirst([
			'conditions' => 'id = :id: ',
			'bind'       => [
				'id' => $id,
			]]);

		$form = new SubjectsForm($subjects);
		$this->view->setVars([
			'form'     => $form,
			'subjects' => $subjects,
		]);
		if ( !$this->request->isPost() ) {
			return $this->view;
		}
		if ( $form->isValid($this->request->getPost(), $subjects) ) {

			if ( $subjects->save() ) {
				return $this->response->redirect('/subjects', true);
			}
		}
	}

	public function delAction()
	{
		$id       = $this->dispatcher->getParam('id');
		$subjects = Subject::findFirst([
			'conditions' => 'id = :id: ',
			'bind'       => [
				'id' => $id,
			]]);
		$subjects->delete();

		return $this->response->redirect('/subjects', true);
	}

}


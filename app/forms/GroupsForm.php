<?php
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\Date;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex as RegexValidator;
use Phalcon\Validation\Validator\StringLength;

class GroupsForm extends \Phalcon\Forms\Form
{
	public function initialize($entity = null, $options = null)
	{

		$name = new Text("name");

		$name->addValidator(
			new StringLength([
				'max'            => 15,
				'min'            => 2,
				'messageMaximum' => 'Название не может содержать больше 15 символов',
				'messageMinimum' => 'Название не может содержать меньше 5 символов',
			]));

		$name->addValidator(
			new RegexValidator([
				'pattern' => '/^[а-яА-Я0-9_-]+$/u',
				'message' => 'Название может содержать только русские буквы, цифры и тире',
			]));

		$this->add($name);

		$today = date("Y");

		$year = new \Phalcon\Forms\Element\Numeric("year", [
			"min"   => 1900,
			"max"   => 2099,
			"step"  => 1,
			"value" => $today,
		]);

		$this->add($year);

		$status = new \Phalcon\Forms\Element\Select("status",
			[
				1 => "Не выпущена",
				0 => "Выпущена",
			]
		);

		$this->add($status);
	}
}
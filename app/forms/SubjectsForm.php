<?php
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\Date;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex as RegexValidator;
use Phalcon\Validation\Validator\StringLength;

class SubjectsForm extends \Phalcon\Forms\Form
{
    public function initialize($entity = null, $options = null)
    {

	    $name = new Text("name");

	    $name->addValidator(
            new StringLength([
                'max' => 50,
                'min' => 5,
                'messageMaximum' => 'Название не может содержать больше 50 символов',
                'messageMinimum' => 'Название не может содержать меньше 5 символов'
            ]));

	    $name->addValidator(
            new RegexValidator([
                'pattern' => '/^[а-яА-Я\s]+$/u',
                'message' => 'Название может содержать только русские буквы'
            ]));

        $this->add($name);

    }
}
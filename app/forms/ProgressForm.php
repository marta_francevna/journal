<?php
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\Date;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex as RegexValidator;
use Phalcon\Validation\Validator\StringLength;

class ProgressForm extends \Phalcon\Forms\Form
{
	public function initialize($entity = null, $options = null)
	{
		$user_id = new \Phalcon\Forms\Element\Hidden("user_id",
			[
				"size"      => 20,
				"maxlength" => 30,
			]);


		$this->add($user_id);


		$subject_id = new \Phalcon\Forms\Element\Select("subject_id", Subject::find(), [
			"using" => [
				"id",
				"name",
			],
		]);

		$this->add($subject_id);

		$group_id = new \Phalcon\Forms\Element\Hidden("group_id",
			[
				"size"      => 20,
				"maxlength" => 30,
			]);

		$this->add($group_id);

		$grade = new Text("grade",
			[
				"size"      => 20,
				"maxlength" => 30,
			]);

		$grade->addValidator(
			new RegexValidator([
				'pattern' => '/[0-9.,]+$/u',
				'message' => 'Поле со средним баллом может содержать только целые или дробные числа',
				]));

	    $this->add($grade);

		$omission = new Text("omission",
			[
				"size"      => 20,
				"maxlength" => 30,
			]);

	    $omission->addValidator(
		    new RegexValidator([
			    'pattern' => '/[0-9]+$/u',
			    'message' => 'Поле с пропусками может содержать только целые числа',
			    ]));

	    $this->add($omission);

	    $semester = new \Phalcon\Forms\Element\Hidden("semester",
		    [
			    "size"      => 20,
			    "maxlength" => 30,
		    ]);

	    $this->add($semester);

		$course = new \Phalcon\Forms\Element\Hidden("course",
			[
				"size"      => 20,
				"maxlength" => 30,
			]);

	    $this->add($course);


    }
}
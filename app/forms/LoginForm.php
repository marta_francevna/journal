<?php

use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\Regex as RegexValidator;
use Phalcon\Validation\Validator\StringLength;

class LoginForm extends \Phalcon\Forms\Form
{
    public function initialize($entity = null, $options = null)
    {

        $login = new Text("login",
            [
                "size" => 20,
                "maxlength" => 30,
            ]);

        $login ->addValidator(
            new RegexValidator([
                'pattern' => '/[a-zA-Z-_0-9]+$/u',
                'message' => 'Логин может содержать только английские буквы или цифры'
            ]));

        $this->add($login);

        $password = new Password("password",[
            "size"=>20,
            "maxlength"=>30,
        ]);
        $password->addValidator(
            new StringLength([
                'min' => 2,
                'messageMinimum' => 'Пароль не может содержать меньше 2 символов'
            ]));

        $password->addFilter('trim');

        $this->add($password);
    }

}
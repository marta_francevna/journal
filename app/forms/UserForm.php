<?php
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\Date;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex as RegexValidator;
use Phalcon\Validation\Validator\StringLength;

class UserForm extends \Phalcon\Forms\Form
{
	public function initialize($entity = null, $options = null)
	{
		$today    = date("Y");
		$group_id = new \Phalcon\Forms\Element\Select("group_id", Group::query()
			->columns(['id', "name"])
			->distinct('name')
			->where("year = :year:")
			->bind(['year' => $today])
			->orderBy('name DESC')
			->execute(),
			[
				"using" => [
					"id",
					"name",
				],
			]);

		$this->add($group_id);

		$name = new Text("name");

		$name->addValidator(
			new RegexValidator([
				'pattern' => '/^[а-яА-Я\s]+$/u',
				'message' => 'ФИО может содержать только русские буквы',
			]));

		$name->addValidator(
			new StringLength([
				'max'            => 50,
				'min'            => 5,
				'messageMaximum' => 'ФИО не может содержать больше 50 символов',
				'messageMinimum' => 'ФИО не может содержать меньше 5 символов',
			]));

		$this->add($name);

		$login = new Text("login");

		$login->addValidator(
			new StringLength([
				'max'            => 50,
				'min'            => 5,
				'messageMaximum' => 'Логин не может содержать больше 50 символов',
				'messageMinimum' => 'Логин не может содержать меньше 5 символов',
			]));

		$login->addValidator(
			new RegexValidator([
				'pattern' => '/^[a-zA-Z0-9_-]+$/u',
				'message' => 'Логин может содержать только английские буквы и цифры',
			]));

		$this->add($login);

		$password = new Password("password");

		$password->addValidator(
			new StringLength([
				'min'            => 5,
				'messageMinimum' => 'Пароль не может содержать меньше 5 символов',
			]));

		$password->addFilter('trim');

		$this->add($password);

		$number = new Text("number");

		$number->addValidator(
			new RegexValidator([
				'pattern' => '/^[0-9-]+$/u',
				'message' => 'Номер студенческого может содержать только цифры',
			]));

		$this->add($number);

		$role = new \Phalcon\Forms\Element\Hidden("role");

		$this->add($role);

		$status = new \Phalcon\Forms\Element\Select("status",
			[
				1 => "Учится",
				0 => "Отчислен",
			]
		);

		$this->add($status);
	}
}
<?php
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\Date;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex as RegexValidator;
use Phalcon\Validation\Validator\StringLength;

class UsersForm extends \Phalcon\Forms\Form
{
    public function initialize($entity = null, $options = null)
    {
        $group_id = new \Phalcon\Forms\Element\Hidden("group_id");

        $this->add($group_id);

        $name = new \Phalcon\Forms\Element\Hidden("name");

        $this->add($name);

        $login = new Text("login");

        $login->addValidator(
            new PresenceOf(
                [
                    "message" => "Поле Login обязательно для заполнения",
                ]
            )
        );

        $login->addValidator(
            new StringLength([
                'max' => 50,
                'min' => 5,
                'messageMaximum' => 'Логин не может содержать больше 50 символов',
                'messageMinimum' => 'Логин не может содержать меньше 5 символов'
            ]));

        $login->addValidator(
            new RegexValidator([
                'pattern' => '/^[a-zA-Z0-9_-]+$/u',
                'message' => 'Логин может содержать только английские буквы'
            ]));

        $this->add($login);

        $password = new Password("password");

        $password->addValidator(
            new StringLength([
                'min' => 5,
                'messageMinimum' => 'Пароль не может содержать меньше 5 символов'
            ]));

        $password->addFilter('trim');

        $this->add($password);

        $number = new \Phalcon\Forms\Element\Hidden("number");

        $this->add($number);

        $role = new \Phalcon\Forms\Element\Hidden("role");

        $this->add($role);

	    $status = new \Phalcon\Forms\Element\Hidden("status");

	    $this->add($status);


    }
}
/**
 * Created by marta on 17.04.17.
 */
$(function() {

    $('#loginform').submit(function(e) {
        e.preventDefault();
        var login = $("#login").val();
        var password = $("#password").val();
        $.ajax({
            type: "POST",
            url: "/login",
            data: {"login": login, "password": password},
            cache: false,
            success: function(response){
                var messageResp2 = new Array('Неправильно введены данные!','Неправильный логин или пароль', "Введите логин и пароль!");
                var resultStat = messageResp2[Number(response)];
                if(response == 3){
                    window.location.href = "/info";
                }else {
                    $("#resp1").text(resultStat).show().delay(1500).fadeOut(800);
                }


            }
        });
    });
});

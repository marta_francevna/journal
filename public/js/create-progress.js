/**
 * Created by marta on 17.04.17.
 */
$(function () {

	$("#b1").click(function (e) {
		e.preventDefault();
		if ( $(this).val() == 0 ) {
			return false;
		}
		var group = $('#group').val();
		var year  = $('#year').val();
		if ( group == 0 ) {
			$("#response").text("Выбери группу").show().delay(1500).fadeOut(800);
			return false;
		}
		$.ajax({
			type    : "POST",
			url     : "/progress/select",
			data    : {"group" : group, "year" : year},
			cache   : false,
			success : function (response) {
				if ( response != 0 ) {
					$("#resp1").fadeOut(300);
					var resp      = document.getElementById('resp2');
					var p         = document.createElement("p");
					var form      = document.getElementById('form');
					p.textContent = 'Выберите ученика:';
					resp.appendChild(p);
					var se         = document.createElement("select");
					se.name        = 'user_id';
					se.id          = 'user_id';
					se.onmousedown = function () {
						$(':first-child', this).remove();
						this.onmousedown = null
					};
					p.appendChild(se);
					var option  = document.createElement("option");
					option.text = " ";
					se.appendChild(option);
					console.log(response);
					for ( var i = 0; i < response[0].length; i++ ) {
						option      = document.createElement("option");
						option.value = response[0][i].id;
						option.text = response[0][i].name;
						se.appendChild(option);
					}
					$("#respon").css('display',"block")
				} else {
					$("#response").text("Извините,нет такой группы этого года").show().delay(1500).fadeOut(800);
					return false;
				}

			}
		});
	});
});


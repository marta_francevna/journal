/**
 * Created by marta on 17.04.17.
 */
$(function() {

    $("#course").click(function(e){
        e.preventDefault();
        if($(this).val() == 0) return false;
        var course  = $(this).val();
	    var resp =  document.getElementById('resp1');
	    var form =  document.getElementById('form');
	   var inp = $('#inp');
        $.ajax({
            type: "POST",
            url: "/info",
            data: {"course": course},
            cache: false,
            success: function(response){
                    removeChildren(resp);
                $('.table').fadeOut(300);
                $('.table2').fadeOut(300);
	            $('.b-text').fadeOut(300);
	            $('.sorry').fadeOut(300);
                var p = document.createElement("span");
                    p.textContent='Выберите семестр:';
                    resp.appendChild(p);
                    var select = document.createElement("select");
                    select.name = 'semester';
                    p.appendChild(select);
                for (var i = 0; i < response[0].length; i++){
                    var option = document.createElement("option");
                    option.text = response[0][i].semester;
                    select.appendChild(option);
               }
                $('.b-button').append('<button class="button" type="submit">Показать</button>');

            }
        });
    });
});

function removeChildren(node) {
    var children = node.childNodes;

    for(var i=0;i<children.length; i++) {
        var child = children[i];
        node.removeChild(child)
    }
}

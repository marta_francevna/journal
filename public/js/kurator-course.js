/**
 * Created by marta on 17.04.17.
 */
$(function () {

	$("#b1").click(function (e) {
		e.preventDefault();
		if ( $(this).val() == 0 ) {
			return false;
		}
		var group = $('#group').val();
		var year  = $('#year').val();
		if ( group == 0 ) {
			$("#response").text("Выбери группу").show().delay(1500).fadeOut(800);
			return false;
		}

		$.ajax({
			type    : "POST",
			url     : "/course",
			data    : {"group" : group, "year" : year},
			cache   : false,
			success : function (response) {
				if ( response != 0 ) {
					$("#resp1").fadeOut(300);
					var resp      = document.getElementById('resp2');
					var p         = document.createElement("span");
					var form      = document.getElementById('form');
					p.textContent = 'Выберите курс:';
					resp.appendChild(p);
					var se         = document.createElement("select");
					se.name        = 'cur';
					se.id          = 'cur';
					se.onmousedown = function () {
						$(':first-child', this).remove();
						this.onmousedown = null
					};
					p.appendChild(se);
					var option  = document.createElement("option");
					option.text = " ";
					se.appendChild(option);
					for ( var i = 0; i < response[0].length; i++ ) {
						option      = document.createElement("option");
						option.text = response[0][i].course;
						se.appendChild(option);
					}
					var inp   = document.createElement("input");
					var div   = document.createElement("div");
					div.className = "b-button";
					inp.type  = "button";
					inp.value = "Далее";
					inp.className = "button";
					inp.addEventListener('click', semesters);
					inp.id = 'inp';
					div.appendChild(inp);
					p.appendChild(div);
				} else {
					$("#response").text("Извините,нет данных").show().delay(1500).fadeOut(800);
					return false;
				}

			}
		});
	});
});


function semesters(e) {
	e.preventDefault();
	if ( $(this).val() == 0 ) {
		return false;
	}
	var course = $('#cur').val();
	var group  = $('#group').val();
	var year   = $('#year').val();
	if ( course == 0 ) {
		$("#response").text("Выбери курс").show().delay(1500).fadeOut(800);
		return false;
	}
	$.ajax({
		type    : "POST",
		url     : "/semesters",
		data    : {"course" : course, "group" : group, "year" : year},
		cache   : false,
		success : function (response) {
			if ( response != 0 ) {
				$("#resp2").fadeOut(500);
				var resp      = document.getElementById('resp3');
				var p         = document.createElement("span");
				var form      = document.getElementById('form');
				p.textContent = 'Выберите семестр:';
				resp.appendChild(p);
				var sel         = document.createElement("select");
				sel.name        = 'sem';
				sel.id          = 'sem';
				sel.onmousedown = function () {
					$(':first-child', this).remove();
					this.onmousedown = null
				};
				p.appendChild(sel);
				var option  = document.createElement("option");
				option.text = " ";
				sel.appendChild(option);
				for ( var i = 0; i < response[0].length; i++ ) {
					option      = document.createElement("option");
					option.text = response[0][i].semester;
					sel.appendChild(option);
				}
				var inp   = document.createElement("input");
				var div   = document.createElement("div");
				div.className = "b-button";
				inp.type  = "submit";
				inp.value = "Далее";
				inp.className = "button";
				inp.addEventListener('click', fuck);
				inp.id = 'inp';
				div.appendChild(inp);
				p.appendChild(div);
			} else {
				$("#response").text("Извините,нет данных").show().delay(1500).fadeOut(800);
				return false;
			}
		}

	});

}

function fuck(e) {
	var sem = $('#sem').val();
	if ( sem == 0 ) {
		e.preventDefault();
		$("#response").text("Выбери семестр").show().delay(1500).fadeOut(800);
		return false;
	}


}
